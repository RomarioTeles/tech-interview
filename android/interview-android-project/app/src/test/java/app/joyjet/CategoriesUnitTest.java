package app.joyjet;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.InputStream;
import java.util.List;

import app.joyjet.activities.FeedActivity;
import app.joyjet.dtos.Category;
import app.joyjet.dtos.Item;
import app.joyjet.http.Client;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoriesUnitTest {

    @Mock
    Context mMockContext;

    static String json_categories;

    static List<Category> categories;

    static Item item;

    @Test
    public void Test1ClientDoGetAsString(){
        String url = "https://cdn.joyjet.com/tech-interview/mobile-test-one.json";
        json_categories = Client.doGetAsString(url);
    }

    @Test
    public void Test2GetCategories() throws Exception {
        assertNotNull(json_categories);
    }

    @Test
    public void Test3DeserializeCategories() throws Exception {
        categories = Category.listFromJson(json_categories);
        assertFalse(categories.isEmpty());
    }

    @Test
    public void Test4EqualsSerializedCategories() throws Exception {
        Gson gson = new Gson();
        String jsonCategories = gson.toJson(categories);
        assertEquals(json_categories, jsonCategories);
    }

    @Test
    public void Test5SerializeItemCategory() throws Exception {
        item = categories.get(0).getItems().get(0);
        assertNotNull(item.toJson());
    }

    @Test
    public void Test6ClientDoGetAsStream() throws Exception {
        String url = item.getGalery().get(0);
        InputStream in = Client.doGetAsInputStream(url);
        assertNotNull(in);
    }
}