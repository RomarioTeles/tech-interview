package app.joyjet.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.List;

import app.joyjet.activities.IRequestImageItem;
import app.joyjet.activities.RequestImagesTask;
import app.test.joyjet.R;

/**
 * Adapter da galeria de imagens do item do feed
 */
public class GaleryPagerAdapter extends PagerAdapter {

    private List<String> galery;

    private Drawable[] drawables;

    private Context context;

    public GaleryPagerAdapter(Context context, List<String> galery) {
        this.context = context;
        this.galery = galery;
        drawables = new Drawable[galery.size()];
    }

    @Override
    public int getCount() {
        return galery.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        String current = galery.get(position);
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.galery_adapter, container, false);
        final ProgressBar progressBar = layout.findViewById(R.id.progressBar);
        final ImageView imageView = layout.findViewById(R.id.image_item);
        IRequestImageItem iRequestImageItem = new IRequestImageItem() {
            @Override
            public void OnPostExecute(Drawable drawable) {
                imageView.setBackground(drawable);
                drawables[position] = drawable;
                progressBar.setVisibility(View.GONE);
            }
        };
        if(drawables[position] == null) {
            RequestImagesTask task = new RequestImagesTask(iRequestImageItem, context);
            task.execute(current);
        }else{
            imageView.setBackground(drawables[position]);
        }
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
