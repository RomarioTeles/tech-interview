package app.joyjet.dtos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Category implements Serializable{

    @Expose
    private String category;

    @Expose
    private List<Item> items;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Item> getItems() {
        if(items == null)
            items = new ArrayList<>();
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public static List<Category> listFromJson(String json){
        Gson gson = new Gson();
        Category[] array = gson.fromJson(json, Category[].class);
        return Arrays.asList(array);
    }

    public static Category fromJson(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, Category.class);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category1 = (Category) o;
        return Objects.equals(category, category1.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(category);
    }

    public void copyCategoryToItems() {
        for(Item item : getItems()){
            item.setCategory(category);
        }
    }

    public String toJson(){
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();
        return gson.toJson(this);
    }

    public static Category findCategory(String category, List<Category> categories){
        Category target = null;
        for(Category item : categories){
            if(item.category.equals(category)){
                target = item;
                break;
            }
        }
        return target;
    }
}
