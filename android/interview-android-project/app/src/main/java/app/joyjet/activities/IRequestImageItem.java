package app.joyjet.activities;

import android.graphics.drawable.Drawable;

/**
 * A classe que chamar a task RequestImagesTask, deve implementar essa interface
 */
public interface IRequestImageItem {

    /**
     * Este metodo contera a logica a ser executada após a imagem ser carregada
     * @param drawables
     */
    void OnPostExecute(Drawable drawables);
}
