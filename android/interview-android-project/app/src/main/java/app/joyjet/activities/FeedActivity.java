package app.joyjet.activities;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import app.joyjet.Utils;
import app.joyjet.adapters.CategoryAdapter;
import app.joyjet.database.DataBaseContract;
import app.joyjet.database.DataSourceHelper;
import app.joyjet.dtos.Category;
import app.joyjet.dtos.Item;
import app.joyjet.http.Client;
import app.test.joyjet.R;


/**
 * FeedActivity é a activite inicial do app
 * esta activity contem todos as noticiais que são carregadas do json
 */
public class FeedActivity extends BaseActivity {

    private ProgressBar progressBar;

    private ListView listViewCategories;

    private CategoryAdapter adapter;

    private List<Category> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_feed);
        progressBar = findViewById(R.id.progressBar);
        /*
        Aqui verifica quais noticias o usuário quer ver, se o usuário selecionar no menu, a opção Favorites,
        uma flag chamada favorite é passada como parametro para indicar sua escolha.
         */
        boolean favorites = getIntent().getBooleanExtra("favorites", false);
        listViewCategories = findViewById(R.id.category_items);
        setTitle(favorites ? "Favorites" : "Digital Space");

        /*
        chama a task responsavel por carregar o feed
         */
        new RequestFeedItemsTask(this).execute(favorites);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean useToolbar() {
        return true;
    }

    @Override
    public boolean useFabButton() {
        return false;
    }

    public List<Category> loadCategoryFromDataBase(){
        List<Category> categories = new ArrayList<>();
        DataSourceHelper mDataSourceHelper = new DataSourceHelper(FeedActivity.this);
        //Os itens favoritos são salvos em um banco sqlite, DataSourceHelper é uma classe utilitaria que possui os metodos
        //que podem ajudar nas tarefas de manipulações de dados.
        String[] projection = new String[]{
                DataBaseContract.Favorites._ID,
                DataBaseContract.Favorites.COLUMN_NAME_JSON,
        };

        Cursor cursor = mDataSourceHelper.query(DataBaseContract.Favorites.TABLE_NAME, projection, null, null, null);
        List<Item> items = new ArrayList<>();
        if(cursor.moveToNext()) {
            String json = cursor.getString(
                    cursor.getColumnIndexOrThrow(DataBaseContract.Favorites.COLUMN_NAME_JSON)
            );

            Item item = Item.fromJson(json);

            if(item != null){
                items.add(item);
            }
        }
        cursor.close();
        for(Item item : items){
            Category category = Category.findCategory(item.getCategory(), categories);
            if(category == null) {
                category = new Category();
                category.setCategory(item.getCategory());
                category.getItems().add(item);
                categories.add(category);
            }else{
                category.getItems().add(item);
            }
        }
        return categories;
    }

    class RequestFeedItemsTask extends AsyncTask{

        private FeedActivity feedActivity;

        private boolean favorites;

        public RequestFeedItemsTask(FeedActivity feedActivity) {
            this.feedActivity = feedActivity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if(o == null || ((List<Category>) o).isEmpty()) {
                if(favorites) {
                    Utils.makeAlertDialog(FeedActivity.this, "Oops!", "No Favorites");
                }else{
                    Utils.makeAlertDialog(FeedActivity.this, "Oops!", "Request Failed");
                }
            }else{
                categories = (List<Category>) o;
                for (Category category : categories) {
                    category.copyCategoryToItems();
                }
                adapter = new CategoryAdapter(FeedActivity.this, categories);
                listViewCategories.setAdapter(adapter);
            }
            progressBar.setVisibility(View.GONE);
        }

        @Override
        protected Object doInBackground(Object... params) {
            favorites = (Boolean) params[0];
            List<Category> categories = new ArrayList<>();
            if(favorites){
                categories = FeedActivity.this.loadCategoryFromDataBase();
            }else{
                String url = "https://cdn.joyjet.com/tech-interview/mobile-test-one.json";
                String response = Client.doGetAsString(url);
                categories = Category.listFromJson(response);
            }

            return categories;
        }
    }
}
