package app.joyjet.dtos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Item implements Serializable{
    @Expose
    private String title;
    @Expose
    private String description;
    @Expose
    private List<String> galery;

    public boolean added;

    @Expose
    private boolean favorito;

    @Expose
    private String category;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getGalery() {
        if(galery == null)
            galery = new ArrayList<>();
        return galery;
    }

    public void setGalery(List<String> galery) {
        this.galery = galery;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public static List<Item> listFromJson(String json){
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Item>>(){}.getType();
        return gson.fromJson(json, listType);
    }

    public static Item fromJson(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, Item.class);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(title, item.title) &&
                Objects.equals(category, item.category);
    }

    @Override
    public int hashCode() {

        return Objects.hash(title, category);
    }

    public String toJson(){
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();
        return gson.toJson(this);
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    public boolean isFavorito() {
        return favorito;
    }
}
