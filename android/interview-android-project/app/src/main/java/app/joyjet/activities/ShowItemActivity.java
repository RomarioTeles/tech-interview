package app.joyjet.activities;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import app.joyjet.adapters.GaleryPagerAdapter;
import app.joyjet.database.DataBaseContract;
import app.joyjet.database.DataSourceHelper;
import app.joyjet.dtos.Item;
import app.test.joyjet.R;

/*
ShowItemActivity Exibir um dado item do feed, escolhido pelo usuário
 */
public class ShowItemActivity extends BaseActivity {

    private ProgressBar progressBar;

    private TextView title, description, category;

    private  ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_show_item);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        title = findViewById(R.id.title);

        description = findViewById(R.id.description);

        category = findViewById(R.id.category);

        viewPager = findViewById(R.id.view_pager_galery);

        String source = (String) getIntent().getStringExtra("item");

        fab.setVisibility(View.VISIBLE);
        /**
         * trata a exibição do item na tela
         */
        if(source != null) {
            final Item item = Item.fromJson(source);
            if(item != null) {
                if(item.isFavorito())
                    fab.setVisibility(View.GONE);
                title.setText(item.getTitle());
                setTitle(item.getTitle().toUpperCase());
                description.setText(item.getDescription());
                category.setText(item.getCategory());

                viewPager.setAdapter(new GaleryPagerAdapter(this, item.getGalery()));
                /**
                 * Acao executada ao clicar no botão de favoritar o item
                 */
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressBar.setVisibility(View.VISIBLE);
                        fab.setEnabled(false);
                        item.setFavorito(true);
                        SaveTask task = new SaveTask(ShowItemActivity.this);
                        task.execute(item);

                        Snackbar.make(view, "Favorite Saved", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                });
            }
        }

    }

    @Override
    public boolean useToolbar() {
        return true;
    }

    @Override
    public boolean useFabButton() {
        return true;
    }

    /**
     * Salva um dado item na base de dados
     */
    public static class SaveTask extends AsyncTask {

        private Context context;

        public SaveTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Object... params) {

            try {

                Item item = (Item) params[0];

                ContentValues contentValues = new ContentValues();

                contentValues.put(DataBaseContract.Favorites.COLUMN_NAME_JSON, item.toJson());

                DataSourceHelper mDataSourceHelper = new DataSourceHelper(context);

                mDataSourceHelper.insert(DataBaseContract.Favorites.TABLE_NAME, contentValues);

                mDataSourceHelper.close();
            }catch (IndexOutOfBoundsException e){
                return null;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object params) {
            super.onPostExecute(params);
        }
    }
}
