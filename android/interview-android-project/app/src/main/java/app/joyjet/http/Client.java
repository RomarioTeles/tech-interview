package app.joyjet.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Client {

    public static String doGetAsString(String url){
        OkHttpClient client = new OkHttpClient();
        Request.Builder builder = new Request.Builder();
        builder.url(url);
        Request request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        }catch (Exception e){
            return null;
        }
    }

    public static InputStream doGetAsInputStream(String url){
        OkHttpClient client = new OkHttpClient.Builder()
                //.readTimeout(10, TimeUnit.SECONDS)
                //.writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Request.Builder builder = new Request.Builder();
        builder.url(url);
        Request request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            return response.body().byteStream();
        }catch (Exception e){
            return null;
        }
    }

}
