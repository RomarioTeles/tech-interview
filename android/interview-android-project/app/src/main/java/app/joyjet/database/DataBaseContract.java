package app.joyjet.database;

import android.provider.BaseColumns;

public class DataBaseContract{
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "joyjet.db";

    public static class Favorites implements BaseColumns {
        public static final String TABLE_NAME = "favorites";
        public static final String COLUMN_NAME_JSON = "json";

        public static final String SQL_CREATE_ENTRY =
                "CREATE TABLE IF NOT EXISTS " + Favorites.TABLE_NAME + " (" +
                        Favorites._ID + DataBaseType.INTEGER_TYPE +" PRIMARY KEY AUTOINCREMENT " + DataBaseType.COMMA_SEP +
                        Favorites.COLUMN_NAME_JSON + DataBaseType.TEXT_TYPE +" )";

        public static final String SQL_DELETE_ENTRY =
                "DROP TABLE IF EXISTS " + Favorites.TABLE_NAME;

    }

    public class DataBaseType {
        public static final String COMMA_SEP = ",";
        public static final String TEXT_TYPE = " TEXT ";
        public static final String NONE_TYPE = " NONE ";
        public static final String INTEGER_TYPE = " INTEGER ";
        public static final String REAL_TYPE = " REAL ";
        public static final String BLOB_TYPE = " BLOB ";
        public static final String NUMERIC_TYPE = " NUMERIC ";
        public static final String DATETIME_TYPE=" DATETIME ";
    }
}
