package app.joyjet;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;

import java.io.InputStream;

import app.joyjet.http.Client;

public class Utils {

    public static void makeAlertDialog(Context context, String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(message).setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    public static Drawable loadImageFromUrl(String url) {
        try {
            InputStream in = Client.doGetAsInputStream(url);
            Drawable d = Drawable.createFromStream(in, url);
            in.close();
            return d;
        } catch (Exception e) {
            return null;
        }
    }

}
