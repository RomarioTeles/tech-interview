package app.joyjet.activities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import app.joyjet.Utils;

/**
 * Esta task é responsavel por requisitar as imagens dos itens do feed
 */
public class RequestImagesTask extends AsyncTask {

    IRequestImageItem iRequestImageItem;

    Context context;

    public RequestImagesTask(IRequestImageItem iRequestImageItem, Context context) {
        this.iRequestImageItem = iRequestImageItem;
        this.context = context;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if(o != null)
            iRequestImageItem.OnPostExecute((Drawable) o);
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        try {
            String url = (String) objects[0];
            Drawable drawable = Utils.loadImageFromUrl(url);
            return drawable;
        }catch (Exception e){
            return null;
        }
    }
}
