package app.joyjet.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import app.joyjet.activities.ShowItemActivity;
import app.joyjet.dtos.Category;
import app.joyjet.dtos.Item;
import app.test.joyjet.R;

/**
 * Adapter da exibição das categorias e items do feed
 */
public class CategoryAdapter extends ArrayAdapter<Category> {

    private List<Category> categories;

    public CategoryAdapter(@NonNull Context context, @NonNull List<Category> objects) {
        super(context, R.layout.category_adapter, objects);
        categories = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View viewCategories = convertView;
        if(viewCategories == null)
            viewCategories = LayoutInflater.from(getContext()).inflate(R.layout.category_adapter, parent,false);
        TextView category = viewCategories.findViewById(R.id.category_name);
        category.setText(categories.get(position).getCategory());
        LinearLayout categoryItems = viewCategories.findViewById(R.id.category_items);
        List<Item> items = categories.get(position).getItems();
        for(final Item item : items) {
            if (!item.added) {
                item.added = true;
                View itemLayout = LayoutInflater.from(getContext()).inflate(R.layout.item_adapter, parent, false);
                ViewPager viewPager = itemLayout.findViewById(R.id.view_pager_galery);
                TextView title = itemLayout.findViewById(R.id.title);
                TextView description = itemLayout.findViewById(R.id.description);
                viewPager.setAdapter(new GaleryPagerAdapter(getContext(), item.getGalery()));
                title.setText(item.getTitle());
                description.setText(item.getDescription());
                Button more = itemLayout.findViewById(R.id.more);
                more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), ShowItemActivity.class);
                        intent.putExtra("item", item.toJson());
                        getContext().startActivity(intent);
                    }
                });

                categoryItems.addView(itemLayout);
            }
        }
        return viewCategories;
    }
}
